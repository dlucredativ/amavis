# SPDX-License-Identifier: GPL-2.0-or-later

package Amavis::Lookup::OpaqueRef;
use strict;
use re 'taint';

# Make an object out of the supplied argument, pretocting it
# from being interpreted as an acl- or a hash- type lookup.
# The argument to new() is expected to be a ref to a variable,
# which will be dereferenced by a method get().
#
sub new($$) { my($class,$obj) = @_; bless \$obj, $class }
sub get($) { ${${$_[0]}} }

1;
